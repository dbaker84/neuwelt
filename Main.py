import random
import noise
import math
import numpy as np
import string
import time
import libtcodpy as libtcod
import languagetools as lang
from enum import Enum
import textwrap

start_time = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())

SCREEN_WIDTH = 85
SCREEN_HEIGHT = 50
HEADER_HEIGHT = 5
MLOG_HEIGHT = 7
INV_HEIGHT = SCREEN_HEIGHT - 10
INV_WIDTH = SCREEN_WIDTH - 10

MAP_HEIGHT = 150
MAP_WIDTH = 150

TURN = 1

WORLD_SEED = random.randrange(-10000, 10000)

scale = 200  # default 100
octaves = 5  # default 6
persistence = 1  # default 0.5
lacunarity = 2.0  # default 2.0

REALTIME = False
TOGGLE_MOUSE_LOOK = False
TOGGLE_DURA_ON_LOOK = False
TOGGLE_VNUM_ON_LOOK = False

entities = []
storage = {}

message_x = 2
message_width = SCREEN_WIDTH - 2
message_height = MLOG_HEIGHT - 1

inventory_open = False

langkey = lang.generate_key()

# console defines
libtcod.console_set_custom_font('Anikki.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_ASCII_INROW)
root = libtcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, 'Neuwelt', fullscreen=False)
con = libtcod.console_new(SCREEN_WIDTH, SCREEN_HEIGHT)
header = libtcod.console_new(SCREEN_WIDTH, HEADER_HEIGHT)
mlog = libtcod.console_new(SCREEN_WIDTH, MLOG_HEIGHT)
invcon = libtcod.console_new(INV_WIDTH, INV_HEIGHT)

global key
global mouse
global language
global mouse_xtrue
global mouse_ytrue

class GameStates(Enum):
    PLAYERS_TURN = 1
    AI_TURN = 2
    PLAYER_DEAD = 3

class Message:
    def __init__(self, text, color=libtcod.white):
        self.text = text
        self.color = color

class MessageLog:
    def __init__(self, x, width, height):
        self.messages = []
        self.x = x
        self.width = width
        self.height = height

    def add_message(self, message):
        # Split the message if necessary, among multiple lines
        new_msg_lines = textwrap.wrap(message.text, self.width)

        for line in new_msg_lines:
            # If the buffer is full, remove the first line to make room for the new one
            if len(self.messages) == self.height:
                del self.messages[0]

            # Add the new line as a Message object, with the text and the color
            self.messages.append(Message(line, message.color))

class Entity:
    VNUM_START = 1

    def __init__(self, x, y, name, char, color, dura=100, keyword=[], res=None):
        self.x = x
        self.y = y
        self.name = name
        self.dura = dura
        self.char = char
        self.color = color
        self.keyword = keyword
        self.res = res

        self.vnum = Entity.VNUM_START
        Entity.VNUM_START += 1

    def move(self, dx, dy):
        # move by the given amount, if the destination is not blocked
        if not worldmap[self.x + dx][self.y + dy].block_move:
            self.x += dx
            self.y += dy

    def damage(self, dmg):
        self.dura -= dmg
        if self.dura <= 0:
            ent_remove(self.vnum)

    def draw(self):
        # set the color and then draw the character that represents this object at its position
        libtcod.console_set_default_foreground(con, self.color)
        libtcod.console_put_char(con, (self.x - cursor.x) + (SCREEN_WIDTH // 2),
                                 (self.y - cursor.y) + (SCREEN_HEIGHT // 2), self.char, libtcod.BKGND_NONE)

    def clear(self):
        # erase the character that represents this object
        libtcod.console_put_char(con, (self.x - cursor.x) + (SCREEN_WIDTH // 2),
                                 (self.y - cursor.y) + (SCREEN_HEIGHT // 2), ' ', libtcod.BKGND_NONE)


class Living(Entity):
    def __init__(self, x, y, name, char, color, dura, keyword, res, aitype='default'):
        Entity.__init__(self, x, y, name, char, color, dura, keyword, res)
        self.aitype = aitype

    def taketurn(self):
        dx = random.randint(-1,1)
        dy = random.randint(-1, 1)
        if not worldmap[self.x + dx][self.y + dy].block_move:
            self.x += dx
            self.y += dy
        # if self.aitype == 'grazer':
        #     print("%s is grazing." % self.name)
        # else:
        #     print("%s stands there doing nothing." % self.name)


class Tile:
    # a tile of the map and its properties
    def __init__(self, name='', char='.', fg=libtcod.white, bg=libtcod.black, block_move=False, block_sight=False):
        self.name = name
        self.char = char
        self.fg = fg
        self.bg = bg
        self.block_move = block_move
        self.block_sight = block_sight

def language_gen():
    language = []
    for i in range(0,26):
        language.append(random.choice(string.ascii_lowercase))


def ent_remove(vnum):
    for index, item in enumerate(entities):
        if item.vnum == vnum:
            break
        else:
            index = -1
    if index >= 0:
        entities.__delitem__(index)


def clear_header():
    libtcod.console_clear(header)
    #header.draw_str(1, 1, "This is the header", libtcod.grey, (0, 0, 0))
    libtcod.console_put_char_ex(header, 0, 0, 218, libtcod.sky, libtcod.black)
    libtcod.console_put_char_ex(header, SCREEN_WIDTH - 1, 0, 191, libtcod.sky, libtcod.black)
    libtcod.console_put_char_ex(header, 0, HEADER_HEIGHT - 1, 192, libtcod.sky, libtcod.black)
    libtcod.console_put_char_ex(header, SCREEN_WIDTH - 1, HEADER_HEIGHT - 1, 217, libtcod.sky, libtcod.black)

    for y in range(1, HEADER_HEIGHT - 1):
        libtcod.console_put_char_ex(header, 0, y, 179, libtcod.sky, libtcod.black)
        libtcod.console_put_char_ex(header, SCREEN_WIDTH - 1, y, 179, libtcod.sky, libtcod.black)
    for x in range(1, SCREEN_WIDTH-1):
        libtcod.console_put_char_ex(header, x, 0, 196, libtcod.sky, libtcod.black)
        libtcod.console_put_char_ex(header, x, HEADER_HEIGHT - 1, 196, libtcod.sky, libtcod.black)

    # for x in range(1, SCREEN_WIDTH-1):
    #     for y in range(1, HEADER_HEIGHT - 1):
    #         libtcod.console_put_char_ex(header, x, y, ' ', libtcod.grey, libtcod.black)


def clear_mlog():
    libtcod.console_put_char_ex(mlog, 0, 0, 218, libtcod.sky, libtcod.black)
    libtcod.console_put_char_ex(mlog, SCREEN_WIDTH - 1, 0, 191, libtcod.sky, libtcod.black)
    # libtcod.console_put_char_ex(mlog, 0, MLOG_HEIGHT - 1, 192, libtcod.sky, libtcod.black)
    # libtcod.console_put_char_ex(mlog, SCREEN_WIDTH - 1, MLOG_HEIGHT - 1, 217, libtcod.sky, libtcod.black)

    for y in range(1, MLOG_HEIGHT):
        libtcod.console_put_char_ex(mlog, 0, y, 179, libtcod.sky, libtcod.black)
        libtcod.console_put_char_ex(mlog, SCREEN_WIDTH - 1, y, 179, libtcod.sky, libtcod.black)
    for x in range(1, SCREEN_WIDTH-1):
        libtcod.console_put_char_ex(mlog, x, 0, 196, libtcod.sky, libtcod.black)
        # libtcod.console_put_char_ex(mlog, x, MLOG_HEIGHT - 1, 196, libtcod.sky, libtcod.black)

    for x in range(1, SCREEN_WIDTH-1):
        for y in range(1, MLOG_HEIGHT - 1):
            libtcod.console_put_char_ex(mlog, x, y, ' ', libtcod.grey, libtcod.black)


def event_handler():
    global TOGGLE_DURA_ON_LOOK
    global TOGGLE_MOUSE_LOOK
    global TOGGLE_VNUM_ON_LOOK
    global inventory_open

    key_char = chr(key.c)

    # Movement keys
    if key.vk == libtcod.KEY_UP or key_char == 'w':
        return {'move': (0, -1)}
    elif key.vk == libtcod.KEY_DOWN or key_char == 'x':
        return {'move': (0, 1)}
    elif key.vk == libtcod.KEY_LEFT or key_char == 'a':
        return {'move': (-1, 0)}
    elif key.vk == libtcod.KEY_RIGHT or key_char == 'd':
        return {'move': (1, 0)}
    elif key_char == 'q':
        return {'move': (-1, -1)}
    elif key_char == 'e':
        return {'move': (1, -1)}
    elif key_char == 'z':
        return {'move': (-1, 1)}
    elif key_char == 'c':
        return {'move': (1, 1)}
    elif key_char == 'p':
        TOGGLE_MOUSE_LOOK = not TOGGLE_MOUSE_LOOK
    elif key_char == 'l':
        TOGGLE_DURA_ON_LOOK = not TOGGLE_DURA_ON_LOOK
    elif key_char == ',':
        TOGGLE_VNUM_ON_LOOK = not TOGGLE_VNUM_ON_LOOK
    elif key.vk == libtcod.KEY_SPACE:
        return {'extract': True }
    elif key_char == 'i':
        inventory_open = not inventory_open

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        # Alt+Enter: toggle full screen
        return {'fullscreen': True}
    elif key.vk == libtcod.KEY_ESCAPE:
        # Exit the game
        return {'exit': True}

    # No key was pressed
    return {}


def get_names_at_coord(x, y):
    names = []
    objname = ''
    for obj in entities:
        if obj.x == x and obj.y == y and obj.name != 'Cursor':
            objname = obj.name
            if TOGGLE_DURA_ON_LOOK:
                objname = objname + '(' + str(obj.dura) + ')'
            if TOGGLE_VNUM_ON_LOOK:
                objname = objname + '[' + str(obj.vnum) + ']'

            names.append(objname)

    namelist = ', '.join(names)

    return namelist


def generate_new_map():
        global worldmap
        global world_noise


        shape = (MAP_WIDTH, MAP_HEIGHT)


        # for g in range(shape[0]):
        #     for h in range(shape[1]):
        #         worldmap[g][h] = Tile('Blank',' ', (0,0,0), (0,0,0))

        worldmap = np.zeros(shape, Tile)

        world = np.zeros(shape)
        for i in range(shape[0]):
            for j in range(shape[1]):
                world[i][j] = noise.snoise2(i / scale + WORLD_SEED,
                                            j / scale + WORLD_SEED,
                                            octaves=octaves,
                                            persistence=persistence,
                                            lacunarity=lacunarity,
                                            repeatx=1024,
                                            repeaty=1024,
                                            base=0)

        center_x, center_y = shape[1] // 2, shape[0] // 2
        circle_grad = np.zeros_like(world)

        for y in range(world.shape[0]):
            for x in range(world.shape[1]):
                distx = abs(x - center_x)
                disty = abs(y - center_y)
                dist = math.sqrt(distx*distx + disty*disty)
                circle_grad[y][x] = dist

        # get it between -1 and 1
        max_grad = np.max(circle_grad)
        circle_grad = circle_grad / max_grad
        circle_grad -= 0.5
        circle_grad *= 2.0
        circle_grad = -circle_grad

        # shrink gradient
        for y in range(world.shape[0]):
            for x in range(world.shape[1]):
                if circle_grad[y][x] > 0:
                    circle_grad[y][x] *= 20

        # get it between 0 and 1
        max_grad = np.max(circle_grad)
        circle_grad = circle_grad / max_grad

        world_noise = np.zeros_like(world)

        for i in range(shape[0]):
            for j in range(shape[1]):
                world_noise[i][j] = (world[i][j] * circle_grad[i][j])
                if world_noise[i][j] > 0:
                    world_noise[i][j] *= 20

        # get it between 0 and 1
        max_grad = np.max(world_noise)
        world_noise = world_noise / max_grad


        #check for land along perimeter
        for i in range(shape[0] // 5):
            for j in range(shape[1] // 5):
                if world_noise[MAP_WIDTH - i - 1][MAP_HEIGHT - j - 1] > 0.25 or \
                world_noise[MAP_WIDTH - i - 1][j] > 0.25 or \
                world_noise[i][MAP_HEIGHT - j - 1] > 0.25 or \
                world_noise[i][j] > 0.25:
                    print("MAP FAILED - Land on perimeter")
                    return False

        #check for enough land mass
        LandCount = 0
        SeaCount = 0
        for i in range(shape[0]):
            for j in range(shape[1]):
                if world_noise[i][j] > 0.25:
                    LandCount += 1
                else:
                    SeaCount += 1
        if LandCount / (LandCount + SeaCount) < 0.07:
            print("MAP FAILED - Not enough land")
            return False

        lightblue = libtcod.light_blue
        blue = libtcod.blue
        green = libtcod.green
        darkgreen = libtcod.dark_green
        crag = libtcod.dark_yellow
        beach = libtcod.light_yellow
        snow = libtcod.white
        mountain = libtcod.grey
        lair = libtcod.darker_magenta
        abyss = libtcod.darkest_blue

        threshold = 0.2

        # def add_color2(world):
        #     color_world = np.zeros(world.shape+(3,))
        for i in range(shape[0]):
            for j in range(shape[1]):
                # world_noise[i][j] += random.uniform(-0.02, 0.02)

                if world_noise[i][j] < threshold + -0.15:
                    worldmap[i][j] = Tile('Ocean',' ', blue, blue, block_move=True)
                elif world_noise[i][j] < threshold + 0.05:
                    worldmap[i][j] = Tile('Shoals',' ', lightblue, lightblue)
                elif world_noise[i][j] < threshold + 0.155:
                    worldmap[i][j] = Tile('Beach',' ', beach, beach)
                elif world_noise[i][j] < threshold + 0.18:
                    worldmap[i][j] = Tile('Crag',' ', crag, crag)
                elif world_noise[i][j] < threshold + 0.35:
                    worldmap[i][j] = Tile('Plains',' ', green, green)
                elif world_noise[i][j] < threshold + 0.6:
                    worldmap[i][j] = Tile('Forest',' ', darkgreen, darkgreen)
                elif world_noise[i][j] < threshold + 0.75:
                    worldmap[i][j] = Tile('Mountain',' ', mountain, mountain, block_move=True)
                elif world_noise[i][j] < threshold + 1.0:
                    worldmap[i][j] = Tile('Snow',' ', snow, snow, block_move=True)

        min = 0
        min_point = (0,0)
        max = 0
        max_point = (0,0)

        for x in range(shape[0]):
            for y in range(shape[1]):
                if world_noise[x][y] > max:
                    max = world_noise[x][y]
                    max_point = (x , y)
                if world_noise[x][y] < min:
                    min = world_noise[x][y]
                    min_point = (x, y)

        worldmap[max_point[0]][max_point[1]] = Tile('Lair', ' ', lair, lair)
        worldmap[min_point[0]][min_point[1]] = Tile('Abyss', ' ', abyss, abyss)


        #print(min, max)

        return True


def plant_trees():
    for i in range(MAP_WIDTH):
        for j in range(MAP_HEIGHT):
            if worldmap[j][i].name == "Forest":
                if random.randrange(1, 100) < 50:
                    entities.append(Entity(j, i, 'Tree', 6, libtcod.darker_green, random.randint(20, 100), ['RES'], 'LUMBER'))
            elif worldmap[j][i].name == "Plains":
                if random.randrange(1, 100) < 10:
                    entities.append(Entity(j, i, 'Tree', 5, libtcod.darker_green, random.randint(20, 100), ['RES'], 'LUMBER'))


def plant_critters():
    for i in range(MAP_WIDTH):
        for j in range(MAP_HEIGHT):
            if worldmap[j][i].name == "Forest":
                if random.randrange(1, 100) < 20:
                    entities.append(Living(j, i, 'Critter', 235, libtcod.dark_amber, random.randint(20, 100), ['ANI', 'RES'], 'FLESH'))
            elif worldmap[j][i].name == "Plains":
                if random.randrange(1, 100) < 40:
                    entities.append(Living(j, i, 'Critter', 235, libtcod.darker_amber, random.randint(20, 100), ['ANI', 'RES'], 'FLESH', 'grazer'))


def make_cliffs():
    for i in range(MAP_WIDTH):
        for j in range(MAP_HEIGHT):
            if worldmap[j][i].name == "Mountain":
                if random.randrange(1, 100) > 20:
                    entities.append(Entity(j, i, 'Cliff', '^', libtcod.light_gray))


def extract_resource(x, y):
    # find resources at point

    global lumber
    global stone
    global metal

    obj_at_point = []
    res_ent = next((obj for obj in entities if 'RES' in obj.keyword and obj.x == x and obj.y == y), False)
    if res_ent:
        try:
            storage[res_ent.res] += random.randint(1, 3)
        except KeyError:
            storage[res_ent.res] = random.randint(1, 3)

        message_log.add_message(Message("You collect some %s." % res_ent.res.lower()) )
        res_ent.damage(random.randint(30, 50))
    else:
        message_log.add_message(Message("You rest."))


def draw_inv_menu():
    libtcod.console_clear(invcon)
    libtcod.console_put_char_ex(invcon, 0, 0, 218, libtcod.sky, libtcod.black)
    libtcod.console_put_char_ex(invcon, INV_WIDTH - 1, 0, 191, libtcod.sky, libtcod.black)
    libtcod.console_put_char_ex(invcon, 0, INV_HEIGHT - 1, 192, libtcod.sky, libtcod.black)
    libtcod.console_put_char_ex(invcon, INV_WIDTH - 1, INV_HEIGHT - 1, 217, libtcod.sky, libtcod.black)

    for y in range(1, INV_HEIGHT - 1):
        libtcod.console_put_char_ex(invcon, 0, y, 179, libtcod.sky, libtcod.black)
        libtcod.console_put_char_ex(invcon, INV_WIDTH - 1, y, 179, libtcod.sky, libtcod.black)
    for x in range(1, INV_WIDTH-1):
        libtcod.console_put_char_ex(invcon, x, 0, 196, libtcod.sky, libtcod.black)
        libtcod.console_put_char_ex(invcon, x, INV_HEIGHT - 1, 196, libtcod.sky, libtcod.black)

    y = 2
    for key in storage:
        libtcod.console_set_default_foreground(invcon, libtcod.light_grey)
        libtcod.console_print_ex(invcon, 2, y, libtcod.BKGND_NONE, libtcod.LEFT, "%s x %i" % (key, storage[key]))
        y += 1


def render_all(xc, yc):

    starty = yc - ((SCREEN_HEIGHT) // 2)
    startx = xc - (SCREEN_WIDTH // 2)

    xi = -1
    for x in range(startx, startx + SCREEN_WIDTH):
        yi = -1
        xi += 1
        for y in range(starty, starty + SCREEN_HEIGHT):
            yi += 1
            if(x >= MAP_WIDTH or x < 0 or y >= MAP_HEIGHT or y < 0):
                #libtcod.console_put_char_ex(con, xi, yi, 176, libtcod.Color(60, 60, 60), libtcod.Color(0, 0, 0))
                # libtcod.console_set_default_foreground(con, libtcod.white)
                libtcod.console_set_char_background(con, xi, yi, libtcod.darker_magenta, libtcod.BKGND_SET)
            else:
            #     libtcod.console_put_char_ex(con, xi, yi, worldmap[x][y].char, worldmap[x][y].fg, worldmap[x][y].bg)
                libtcod.console_set_char_background(con, xi, yi, worldmap[x][y].bg, libtcod.BKGND_SET)

    # draw all objects in the list
    for object in entities:
        if startx < object.x < startx + SCREEN_WIDTH and starty < object.y < starty + SCREEN_HEIGHT:
            object.draw()

    clear_header()
    clear_mlog()

    if TOGGLE_MOUSE_LOOK:
        libtcod.console_set_default_foreground(header, libtcod.green)
        libtcod.console_print(header, 1, 1, str(mouse_xtrue) + " " + str(mouse_ytrue))
        libtcod.console_print(header, 1, 2, worldmap[mouse_xtrue][mouse_ytrue].name)
        libtcod.console_print(header, 1, 3, get_names_at_coord(mouse_xtrue, mouse_ytrue))
    else:
        libtcod.console_set_default_foreground(header, libtcod.white)
        libtcod.console_print(header, 1, 1, str(cursor.x) + " " + str(cursor.y))
        libtcod.console_print(header, 1, 2, worldmap[cursor.x][cursor.y].name)
        libtcod.console_print(header, 1, 3, get_names_at_coord(cursor.x, cursor.y))

    libtcod.console_set_color_control(libtcod.COLCTRL_1, libtcod.dark_green, libtcod.black)
    libtcod.console_set_color_control(libtcod.COLCTRL_2, libtcod.grey, libtcod.black)
    libtcod.console_set_color_control(libtcod.COLCTRL_3, libtcod.sepia, libtcod.black)
    libtcod.console_print_ex(header, SCREEN_WIDTH - 2, 1, libtcod.BKGND_NONE, libtcod.RIGHT,
                               "%cLumber:%c " % (libtcod.COLCTRL_1, libtcod.COLCTRL_STOP) + str(lumber) +
                             "%c  Stone: %c" % (libtcod.COLCTRL_2, libtcod.COLCTRL_STOP) + str(stone) +
                             "%c  Metal:%c" % (libtcod.COLCTRL_3, libtcod.COLCTRL_STOP) + str(metal))
    libtcod.console_print_ex(header, SCREEN_WIDTH - 2, 2, libtcod.BKGND_NONE, libtcod.RIGHT, str(TURN))

    y = 1
    for message in message_log.messages:
        libtcod.console_set_default_foreground(mlog, message.color)
        libtcod.console_print_ex(mlog, message_log.x, y, libtcod.BKGND_NONE, libtcod.LEFT, message.text)
        y += 1
    

global cursor
cursor = Entity(MAP_WIDTH // 2, MAP_HEIGHT // 2, 'Cursor', 2, libtcod.flame, 100)
entities.append(cursor)

nyctelios = Entity(MAP_WIDTH // 2, MAP_HEIGHT // 2, 'Nyctelios', 234, libtcod.azure, 10000)
entities.append(nyctelios)
# objects.append(Entity(MAP_WIDTH // 2 + 2, MAP_HEIGHT // 2, 'TestChar', 'T', libtcod.green))

MapGenSuccess = False
while not MapGenSuccess:
    MapGenSuccess = generate_new_map()
    # scale += 1
    WORLD_SEED = random.randrange(-10000, 10000)


FindStart = True
while FindStart:
    x = random.randrange(MAP_WIDTH // 4, (MAP_WIDTH // 4) * 3)
    y = random.randrange(MAP_HEIGHT // 4, (MAP_HEIGHT // 4) * 3)
    if not worldmap[x][y].block_move:
        cursor.x = x
        cursor.y = y
        FindStart = False

nyctelios.x = cursor.x + 1
nyctelios.y = cursor.y

language_gen()
plant_trees()
plant_critters()
make_cliffs()


# print(start_time)
# print(end_time)

mouse = libtcod.Mouse()
key = libtcod.Key()

game_state = GameStates.PLAYERS_TURN
message_log = MessageLog(message_x, message_width, message_height)


#initial game setup

lumber = 0
stone = 0
metal = 0

message_log.add_message(Message(str("NYCTELIOS: " + lang.encrypt(langkey, 'Welcome to my island. Behave yourself or I will destroy you.').capitalize()),nyctelios.color))


# MAIN LOOP
while not libtcod.console_is_window_closed():

    libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)
    xoffset = mouse.cx - (SCREEN_WIDTH // 2)
    yoffset = mouse.cy - (SCREEN_HEIGHT // 2)
    mouse_xtrue = xoffset + cursor.x
    mouse_ytrue = yoffset + cursor.y

    render_all(cursor.x, cursor.y)

    cursor.color = libtcod.Color(worldmap[cursor.x][cursor.y].bg.g, worldmap[cursor.x][cursor.y].bg.b, worldmap[cursor.x][cursor.y].bg.r)
    cursor.draw()

    libtcod.console_blit(con, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, root, 0, 0)
    libtcod.console_blit(header, 0, 0, SCREEN_WIDTH, HEADER_HEIGHT, root, 0, 0)
    libtcod.console_blit(mlog, 0, 0, SCREEN_WIDTH, MLOG_HEIGHT, root, 0, SCREEN_HEIGHT - MLOG_HEIGHT)
    if inventory_open:
        draw_inv_menu()
        libtcod.console_blit(invcon, 0, 0, INV_WIDTH, INV_HEIGHT, root, 5, 5)

    libtcod.console_flush()

    for object in entities:
        object.clear()

    action = event_handler()

    move = action.get('move')
    exit = action.get('exit')
    fullscreen = action.get('fullscreen')
    mouselook = action.get('mouselook')
    duralook = action.get('duralook')
    extract = action.get('extract')

    player_turn_results = []

    if move and game_state == GameStates.PLAYERS_TURN:
        dx, dy = move
        if not worldmap[cursor.x + dx][cursor.y + dy].block_move:
            cursor.x += dx
            cursor.y += dy
            game_state = GameStates.AI_TURN

    if exit:
        break

    if extract:
        extract_resource(cursor.x, cursor.y)
        game_state = GameStates.AI_TURN

    if fullscreen:
        libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

    # for player_turn_result in player_turn_results:
    #     message = player_turn_result.get('message')
    #     dead_entity = player_turn_result.get('dead')
    #
    #     if message:
    #         message_log.add_message(message)
    #
    #     if dead_entity:
    #         if dead_entity == player:
    #             message, game_state = kill_player(dead_entity)
    #         else:
    #             message = kill_monster(dead_entity)
    #
    #         message_log.add_message(message)

    if game_state == GameStates.AI_TURN:
        for obj in entities:
            if type(obj) is Living:
                obj.taketurn()

        TURN += 1
        game_state = GameStates.PLAYERS_TURN
        # for entity in entities:
        #     if entity.ai:
        #         enemy_turn_results = entity.ai.take_turn(player, fov_map, game_map, entities)
        #
        #         for enemy_turn_result in enemy_turn_results:
        #             message = enemy_turn_result.get('message')
        #             dead_entity = enemy_turn_result.get('dead')
        #
        #             if message:
        #                 message_log.add_message(message)
        #
        #             if dead_entity:
        #                 if dead_entity == player:
        #                     message, game_state = kill_player(dead_entity)
        #                 else:
        #                     message = kill_monster(dead_entity)
        #
        #                 message_log.add_message(message)
        #
        #                 if game_state == GameStates.PLAYER_DEAD:
        #                     break
        #
        #         if game_state == GameStates.PLAYER_DEAD:
        #             break
